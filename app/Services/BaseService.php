<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\RepositoryInterface;

class BaseService implements BaseServiceInterface
{
    protected $repository;

    public function __construct(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        return $this->repository->all();
    }

    public function show(Model $model)
    {
        return $this->repository->find($model->id);
    }

    public function update($data, Model $model)
    {
        return $this->repository->update($data,  $model->id);
    }

    public function store($data)
    {
        return $this->repository->create($data);
    }

    public function updateOrCreate($attributes, $values)
    {
        return $this->repository->updateOrCreate($attributes, $values);
    }

    public function delete(Model $model)
    {
        return $this->repository->delete( $model->id);
    }

    public function getRepository()
    {
        return $this->repository;
    }
}
