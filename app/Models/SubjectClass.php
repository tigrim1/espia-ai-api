<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class subjectClass extends Model
{
    use HasFactory;
    protected $fillable = ['employee_id', 'subject_id', 'student_class_id'];

    public function rules() {
        return [
            'employee_id' => 'exists:employees,id',
            'subject_id' => 'exists:subjects,id',
            'student_class_id' => 'exists:student_classes,id',
        ];
    }

    public function employee() {
        return $this->belongsTo('App\Models\Employee');
    }

    public function subject() {
        return $this->belongsTo('App\Models\Subject');
    }

    public function studentClass() {
        return $this->belongsTo('App\Models\StudentClass');
    }

    public function contents() {
        return $this->hasMany('App\Models\Content');
    }
}
