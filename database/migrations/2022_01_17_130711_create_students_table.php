<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('school_id');
            $table->unsignedBigInteger('student_class_id');
            $table->string('username', 30)->unique();
            $table->string('password');
            $table->string('matriculation_number');
            $table->string('name', 30);
            $table->string('photo', 100);
            $table->string('email')->unique();
            $table->string('phone');
            $table->timestamps();
            $table->foreign('school_id')->references('id')->on('schools');
            $table->foreign('student_class_id')->references('id')->on('student_classes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
