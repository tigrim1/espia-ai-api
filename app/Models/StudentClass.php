<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentClass extends Model
{
    use HasFactory;
    protected $fillable = ['school_year_id', 'class_number', 'date_year'];

    public function rules() {
        return [
            'school_year_id' => 'exists:school_years,id',
        ];
    }

    public function schoolYear() {
        return $this->belongsTo('App\Models\SchoolYear');
    }

    public function students() {
        return $this->hasMany('App\Models\Student');
    }

    public function subjectClasses() {
        return $this->hasMany('App\Models\SubjectClass');
    }
}
