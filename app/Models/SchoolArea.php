<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SchoolArea extends Model
{
    use HasFactory;
    protected $fillable = ['school_id', 'name', 'description', 'photo'];

    public function rules() {
        return [
            'school_id' => 'exists:schools,id',
            'name' => 'required|unique:schools,name,'.$this->id.'|min:3',
            'photo' => 'file|mimes:png,jpeg,jpg'
        ];
    }

    public function school() {
        return $this->belongsTo('App\Models\School');
    }
}
