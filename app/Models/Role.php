<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;
    protected $fillable = ['group_id', 'name', 'hierarchy'];

    public function rules() {
        return [
            'group_id' => 'exists:groups,id',
            'name' => 'required|unique:roles,name,'.$this->id.'|min:3',
            'hierarchy' => 'required|integer|digits_between:1,20',
        ];
    }

    public function employees() {
        return $this->hasMany('App\Models\Employee');
    }

    public function group() {
        return $this->belongsTo('App\Models\Group');
    }
}
