<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    use HasFactory;
    protected $fillable = ['city_id', 'name', 'public_place', 'public_place_number', 'CEP', 'phone', 'photo'];

    public function rules() {
        return [
            'city_id' => 'exists:cities,id',
            'name' => 'required|unique:schools,name,'.$this->id.'|min:3',
            'photo' => 'file|mimes:png,jpeg,jpg|nullable'
        ];
    }

    public function employees() {
        return $this->hasMany('App\Models\Employee');
    }

    public function schoolAreas() {
        return $this->hasMany('App\Models\SchoolArea');
    }

    public function schoolYears() {
        return $this->hasMany('App\Models\SchoolYear');
    }

    public function students() {
        return $this->hasMany('App\Models\Student');
    }

    public function city() {
        return $this->belongsTo('App\Models\City');
    }
}
