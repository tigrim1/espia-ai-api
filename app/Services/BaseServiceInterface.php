<?php


namespace App\Services;


use Illuminate\Database\Eloquent\Model;

interface BaseServiceInterface
{
    public function index();

    public function show(Model $model);

    public function delete(Model $model);

    public function store($data);

    public function update($data, Model $model);

    public function getRepository();
}
