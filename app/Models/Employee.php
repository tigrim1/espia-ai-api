<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;
    protected $fillable = ['role_id', 'school_id', 'is_teacher', 'username', 'password', 'matriculation_number', 'name',
        'photo', 'email', 'phone'];

    public function rules() {
        return [
            'username' => 'required|unique:employees,username,'.$this->id.'|min:3',
            'school_id' => 'exists:schools,id',
            'role_id' => 'exists:roles,id',
            'is_teacher' => 'boolean',
            'photo' => 'file|mimes:png,jpeg,jpg'
        ];

    }

    public function posts() {
        return $this->hasMany('App\Models\Post');
    }

    public function school(){
        return $this->belongsTo('App\Models\School');
    }

    public function role(){
        return $this->belongsTo('App\Models\Role');
    }
}
