<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Repositories\EmployeeRepository;

class EmployeeController extends Controller
{
    public function __construct(Employee $employee) {
        $this->employee = $employee;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $employeeRepository = new EmployeeRepository($this->employee);

        if($request->has('filtro')) {
            $employeeRepository->filtro($request->filtro);
        }

        if($request->has('atributos')) {
            $employeeRepository->selectAtributos($request->atributos);
        }
        return response()->json($employeeRepository->getResultado(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate($this->employee->rules());
        $photo = $request->file('photo');
        $photo_urn = $photo->store('/', 'public');

        $employee = $this->employee->create([
            'role_id' => $request->role_id,
            'school_id' => $request->school_id,
            'is_teacher' => $request->is_teacher,
            'username' => $request->username,
            'password' => $request->password,
            'matriculation_number' => $request->matriculation_number,
            'name' => $request->name,
            'photo' => $photo_urn,
            'email' => $request->email,
            'phone' => $request->phone,
        ]);
        return response()->json($employee, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employee = $this->employee->with(['school', 'role', 'posts'])->find($id);
        if($employee === null) {
            return response()->json(['erro' => 'Recurso pesquisado não existe'], 404) ;
        }
        return response()->json($employee, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $employee = $this->employee->find($id);

        if($employee === null) {
            return response()->json(['erro' => 'Impossível realizar a atualização. O recurso solicitado não existe'],
                404);
        }

        if($request->method() === 'PATCH') {

            $regrasDinamicas = array();

            //percorrendo todas as regras definidas no Model
            foreach($employee->rules() as $input => $regra) {

                //coletar apenas as regras aplicáveis aos parâmetros parciais da requisição PATCH
                if(array_key_exists($input, $request->all())) {
                    $regrasDinamicas[$input] = $regra;
                }
            }

            $request->validate($regrasDinamicas);

        } else {
            $request->validate($employee->rules());
        }
        if($request->file('photo')) {
            Storage::disk('public')->delete($employee->photo);
        }

        $photo = $request->file('photo');
        $photo_urn = $photo->store('/', 'public');


        $employee->fill($request->all());
        $employee->photo = $photo_urn;
        $employee->save();

        return response()->json($employee, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee = $this->employee->find($id);

        if($employee === null) {
            return response()->json(['erro' => 'Impossível realizar a exclusão. O recurso solicitado não existe'], 404);
        }
        Storage::disk('public')->delete($employee->photo);

        $employee->delete();
        return response()->json(['msg' => 'O modelo foi removido com sucesso!'], 200);

    }
}
