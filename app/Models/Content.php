<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'description','subject_class_id', 'attachment', 'date'];

    public function rules() {
        return [
            'subject_class_id' => 'exists:subject_classes,id',
            'date' => 'required|date',
            'attachment' => 'file|mimes:zip,ppt,pptx,doc,docx,pdf,xls,xlsx|max:204800|nullable',
        ];
    }

    public function subjectClass() {
        return $this->belongsTo('App\Models\SubjectClass');
    }

    public function studentsSawContent() {
        return $this->hasMany('App\Models\StudentSawContent');
    }
}
