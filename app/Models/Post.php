<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;
    protected $fillable = ['employee_id', 'description', 'date', 'photo'];

    public function rules() {
        return [
            'employee_id' => 'exists:employees,id',
            'date' => 'required|date',
            'photo' => 'file|mimes:png,jpeg,jpg|nullable'
        ];
    }

    public function employee() {
        return $this->belongsTo('App\Models\Employee');
    }
}
