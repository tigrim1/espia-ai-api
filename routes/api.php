<?php

use App\Http\Controllers\API\SubscriberController;
use App\Http\Controllers\API\SubscriptionController;
use App\Http\Controllers\API\TagController;
use App\Http\Controllers\API\VideoController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'namespace' => 'App\Http\Controllers',
    'prefix' => 'auth'
], function ($router) {

    Route::post('login', 'Auth\AuthController@login');
    Route::post('logout', 'Auth\AuthController@logout');
    Route::post('refresh', 'Auth\AuthController@refresh');
    Route::put('update', 'Auth\AuthController@update');
    Route::post('authenticate', 'Auth\AuthController@authenticate');
    Route::post('me', 'Auth\AuthController@me');
    Route::get('login/{provider}/{token}', 'Auth\AuthController@handleProviderCallback');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');
});



Route::middleware(['auth:sanctum'])->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'middleware' => ['auth:sanctum']
], function ($router) {
    Route::apiResource('role', 'App\Http\Controllers\RoleController');
    Route::apiResource('employee', 'App\Http\Controllers\EmployeeController');
    Route::apiResource('subject_class', 'App\Http\Controllers\SubjectClassController');
    Route::apiResource('group', 'App\Http\Controllers\GroupController');
    Route::apiResource('city', 'App\Http\Controllers\CityController');
    Route::apiResource('school_area', 'App\Http\Controllers\SchoolAreaController');
    Route::apiResource('content', 'App\Http\Controllers\ContentController');
    Route::apiResource('subject', 'App\Http\Controllers\SubjectController');
    Route::apiResource('student_class', 'App\Http\Controllers\StudentClassController');
    Route::apiResource('school_year', 'App\Http\Controllers\SchoolYearController');
    Route::apiResource('student', 'App\Http\Controllers\StudentController');
    Route::apiResource('student_saw_content', 'App\Http\Controllers\StudentSawContentController');
    Route::apiResource('user', 'App\Http\Controllers\UserController');
    Route::get('get_image/{path}', 'App\Http\Controllers\ImageController@Picture');
    Route::get('get_attachment/{path}', 'App\Http\Controllers\AttachmentController@Attachment')
        ->where('path', '(.*)');

});

Route::apiResource('post', 'App\Http\Controllers\PostController');
Route::apiResource('school', 'App\Http\Controllers\SchoolController');
