<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Http\Request;
use App\Repositories\StudentRepository;
use Illuminate\Support\Facades\Storage;

class StudentController extends Controller
{
    public function __construct(Student $student) {
        $this->student = $student;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $studentRepository = new StudentRepository($this->student);

        if($request->has('filtro')) {
            $studentRepository->filtro($request->filtro);
        }

        if($request->has('atributos')) {
            $studentRepository->selectAtributos($request->atributos);
        }
        return response()->json($studentRepository->getResultado(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate($this->student->rules());
        $photo = $request->file('photo');
        $photo_urn = $photo->store('/', 'public');

        $student = $this->student->create([
            'student_class_id' => $request->student_class_id,
            'school_id' => $request->school_id,
            'username' => $request->username,
            'password' => $request->password,
            'matriculation_number' => $request->matriculation_number,
            'name' => $request->name,
            'photo' => $photo_urn,
            'email' => $request->email,
            'phone' => $request->phone,
        ]);
        return response()->json($student, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $student = $this->student->with(['school', 'studentClass'])->find($id);
        if($student === null) {
            return response()->json(['erro' => 'Recurso pesquisado não existe'], 404) ;
        }
        return response()->json($student, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $student = $this->student->find($id);

        if($student === null) {
            return response()->json(['erro' => 'Impossível realizar a atualização. O recurso solicitado não existe'],
                404);
        }

        if($request->method() === 'PATCH') {

            $regrasDinamicas = array();

            //percorrendo todas as regras definidas no Model
            foreach($student->rules() as $input => $regra) {

                //coletar apenas as regras aplicáveis aos parâmetros parciais da requisição PATCH
                if(array_key_exists($input, $request->all())) {
                    $regrasDinamicas[$input] = $regra;
                }
            }

            $request->validate($regrasDinamicas);

        } else {
            $request->validate($student->rules());
        }
        if($request->file('photo')) {
            Storage::disk('public')->delete($student->photo);
        }

        $photo = $request->file('photo');
        $photo_urn = $photo->store('/', 'public');

        $student->fill($request->all());
        $student->photo = $photo_urn;
        $student->save();

        return response()->json($student, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student = $this->student->find($id);

        if($student === null) {
            return response()->json(['erro' => 'Impossível realizar a exclusão. O recurso solicitado não existe'], 404);
        }
        Storage::disk('public')->delete($student->photo);

        $student->delete();
        return response()->json(['msg' => 'O modelo foi removido com sucesso!'], 200);

    }
}
