<?php

namespace App\Http\Controllers;

use App\Models\StudentClass;
use Illuminate\Http\Request;
use App\Repositories\StudentClassRepository;
use App\Models\Employee;

class StudentClassController extends Controller
{
    public function __construct(StudentClass $student_class) {
        $this->student_class = $student_class;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $student_classRepository = new StudentClassRepository($this->student_class);

        if($request->has('filtro')) {
            $student_classRepository->filtro($request->filtro);
        }

        if($request->has('atributos')) {
            $student_classRepository->selectAtributos($request->atributos);
        }
        return response()->json($student_classRepository->getResultado(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate($this->student_class->rules());
        $student_class = $this->student_class->create([
            'school_year_id' => $request->school_year_id,
            'class_number' => $request->class_number,
            'date_year' => $request->date_year,
        ]);
        return response()->json($student_class, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\StudentClass  $student_class
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $student_class = $this->student_class->with(['schoolYear', 'subjectClasses', 'Students'])->find($id);
        if($student_class === null) {
            return response()->json(['erro' => 'Recurso pesquisado não existe'], 404) ;
        }
        return response()->json($student_class, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\StudentClass  $student_class
     * @return \Illuminate\Http\Response
     */
    public function edit(StudentClass $student_class)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\StudentClass  $student_class
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $student_class = $this->student_class->find($id);

        if($student_class === null) {
            return response()->json(['erro' => 'Impossível realizar a atualização. O recurso solicitado não existe'],
                404);
        }

        if($request->method() === 'PATCH') {

            $regrasDinamicas = array();

            //percorrendo todas as regras definidas no Model
            foreach($student_class->rules() as $input => $regra) {

                //coletar apenas as regras aplicáveis aos parâmetros parciais da requisição PATCH
                if(array_key_exists($input, $request->all())) {
                    $regrasDinamicas[$input] = $regra;
                }
            }

            $request->validate($regrasDinamicas);

        } else {
            $request->validate($student_class->rules());
        }

        $student_class->fill($request->all());
        $student_class->save();

        return response()->json($student_class, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\StudentClass  $student_class
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student_class = $this->student_class->find($id);

        if($student_class === null) {
            return response()->json(['erro' => 'Impossível realizar a exclusão. O recurso solicitado não existe'], 404);
        }

        $student_class->delete();
        return response()->json(['msg' => 'O modelo foi removido com sucesso!'], 200);

    }
}
