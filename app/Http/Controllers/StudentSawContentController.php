<?php

namespace App\Http\Controllers;

use App\Models\StudentSawContent;
use Illuminate\Http\Request;
use App\Repositories\StudentSawContentRepository;
use DB;

class StudentSawContentController extends Controller
{
    public function __construct(StudentSawContent $student_saw_content) {
        $this->student_saw_content = $student_saw_content;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $student_saw_contentRepository = new StudentSawContentRepository($this->student_saw_content);

        if($request->has('filtro')) {
            $student_saw_contentRepository->filtro($request->filtro);
        }

        if($request->has('atributos')) {
            $student_saw_contentRepository->selectAtributos($request->atributos);
        }

        return response()->json($student_saw_contentRepository->getResultado(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate($this->student_saw_content->rules());

        $student_saw_content = $this->student_saw_content->create([
            'student_id' => $request->student_id,
            'content_id' => $request->content_id,
        ]);


        return response()->json($student_saw_content, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\StudentSawContent  $student_saw_content
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $querie = StudentSawContent::with('student')
            ->where('content_id', '=', $id)
            ->get();


        $student_saw_content = $this->student_saw_content->with('student', 'content')->find($id);
        if($student_saw_content === null) {
            return response()->json(['erro' => 'Recurso pesquisado não existe'], 404) ;
        }

        return response()->json($querie, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\StudentSawContent  $student_saw_content
     * @return \Illuminate\Http\Response
     */
    public function edit(StudentSawContent $student_saw_content)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\StudentSawContent  $student_saw_content
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $student_saw_content = $this->student_saw_content->find($id);

        if($student_saw_content === null) {
            return response()->json(['erro' => 'Impossível realizar a atualização. O recurso solicitado não existe'],
                404);
        }

        if($request->method() === 'PATCH') {

            $regrasDinamicas = array();

            //percorrendo todas as regras definidas no Model
            foreach($student_saw_content->rules() as $input => $regra) {

                //coletar apenas as regras aplicáveis aos parâmetros parciais da requisição PATCH
                if(array_key_exists($input, $request->all())) {
                    $regrasDinamicas[$input] = $regra;
                }
            }

            $request->validate($regrasDinamicas);

        } else {
            $request->validate($student_saw_content->rules());
        }

        $student_saw_content->fill($request->all());
        $student_saw_content->save();

        return response()->json($student_saw_content, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\StudentSawContent  $student_saw_content
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student_saw_content = $this->student_saw_content->find($id);

        if($student_saw_content === null) {
            return response()->json(['erro' => 'Impossível realizar a exclusão. O recurso solicitado não existe'], 404);
        }

        $student_saw_content->delete();
        return response()->json(['msg' => 'O modelo foi removido com sucesso!'], 200);

    }
}
